$('#filtro').on('keyup', function () {
   var texto = $(this).val();
   
    $.ajax({
        type: 'POST',
        url: '/index/busca',
        data:{
            texto: texto
        },
        success: function (html) {
            $('#resultado').html(html);
        }
   }) ;
});