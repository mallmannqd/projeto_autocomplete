<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 17/04/18
 * Time: 15:15
 */

namespace models;


use core\Model;

class Usuarios extends Model
{
    private $id;
    private $first_name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    public function buscaUsuario($nome_parcial)
    {
        $sql = "SELECT * FROM usuarios WHERE first_name LIKE :nome_parcial";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':nome_parcial', '%' . $nome_parcial . '%');
        $stmt->execute();

        $result = $stmt->fetchAll();

        if ($result){
            foreach ($result as $nome){
                echo $nome['first_name'] . "<br/>";
            }
        }
    }

}