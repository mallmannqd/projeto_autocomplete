<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;
use models\Usuarios;

class IndexController extends Controller
{
    public function index()
    {
        $this->loadTemplate('index/index');
    }

    public function busca()
    {
        if (!empty($_POST['texto'])){
            $texto = $_POST['texto'];

            $usuario = new Usuarios();
            $usuario->buscaUsuario($texto);

        }

    }

}
